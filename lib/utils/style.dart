import 'dart:ui';
import 'package:flutter/material.dart';



class Styles {
  static Color primaryColor = Color.fromRGBO(35, 161, 125, 1.0);
  static Color accentColor = const Color.fromRGBO(143, 246, 216, 1.0);

}