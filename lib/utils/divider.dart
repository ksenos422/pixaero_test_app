
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Container buildDivider() {
  return Container(
    margin: const EdgeInsets.symmetric(
      horizontal: 8.0,
    ),
    width: double.infinity,
    height: 1.0,
    decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Colors.white,
            Colors.tealAccent,
            Colors.teal,
            Colors.teal,
          ],
        )
    ),
  );
}