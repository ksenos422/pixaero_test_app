import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("О приложении"),
      ),
      body: Container(
        child: Column(
          children: const [
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20, top: 50),
              child: Card(
                color: Colors.teal,
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20), bottomRight: Radius.circular(20))),
                child: ListTile(
                  leading: Icon(Icons.apps, color: Colors.white,),
                  title: Text('Мобильное приложение на Flutter/Dart', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400, fontSize: 22),),
                )
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Card(
                color: Colors.grey,
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: ListTile(
                  title: Text('Идея: api онлайн библиотеки.\nВы также можете добавлять понравившиеся книги к себе в "Избранное"', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300, fontSize: 22),),
                )
              ),
            ),
          ],
        ),
      ),
    );
  }
}