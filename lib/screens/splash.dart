
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app_pixaero/screens/home.dart';
import 'package:test_app_pixaero/utils/style.dart';

import '../main.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with TickerProviderStateMixin {
  late AnimationController motionController;
  late Animation motionAnimation;
  double sizeIcon = 20;

  @override
  void initState() {
    motionController = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
      // lowerBound: 0.7,
    );

    motionAnimation = CurvedAnimation(
      parent: motionController,
      curve: Curves.elasticIn,
    );

    motionController.forward();
    motionController.addStatusListener((status) {
      setState(() {
        if (status == AnimationStatus.completed) {
          motionController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          motionController.forward();
        }
      });
    });

    motionController.addListener(() {
      setState(() {
        sizeIcon = motionController.value * 300;
      });
    });
    Timer(
        Duration(seconds: 2),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => MyHomePage())));
            // builder: (BuildContext context) => HomePage())));
  }

  @override
  void dispose() {
    motionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Styles.primaryColor,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.transparent,
          ),
          child: ListView(
            children: <Widget>[
              const SizedBox(height: 150.0,),
              const Padding(
                padding: EdgeInsets.fromLTRB(45,5,45,5),
                // child: Divider(color: Colors.grey[300],),
              ),
              Center(
                child: Icon(Icons.local_library_outlined, color: Colors.white, size: sizeIcon,),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
