import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app_pixaero/blocs/categories_bloc.dart';
import 'package:test_app_pixaero/blocs/my_book_bloc.dart';
import 'package:test_app_pixaero/models/categories.dart';
import 'package:test_app_pixaero/screens/books_categ.dart';
import 'package:test_app_pixaero/utils/log.dart';
import 'loading_screen.dart';

class CategoriesBooks extends StatelessWidget {
  final CategoriesBloc _myCategoriesBloc = CategoriesBloc();

  @override
  void initState() {
    _myCategoriesBloc.getCategoriess();
  }

  @override
  void dispose() {
    _myCategoriesBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showAddTodoSheet(context);
        },
        child: const Icon(Icons.add),
      ),
      body: Container(
          child: Column(children: [
        StreamBuilder(
          stream: _myCategoriesBloc.categories,
          builder: (BuildContext context,
              AsyncSnapshot<List<Categories>> asyncSnap) {
            if (asyncSnap.hasData) {
              return asyncSnap.data!.length != 0
                  ? ListView.builder(
                      shrinkWrap: true,
                      itemCount: asyncSnap.data!.length,
                      itemBuilder: (context, itemPosition) {
                        Categories categories = asyncSnap.data![itemPosition];
                        return Container(
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: ListTile(
                                title: Text(
                                  '${categories.title}',
                                  textAlign: TextAlign.center,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.teal,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => BooksCateg(
                                        categ_id: categories.idN,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  : Container(
                      child: Center(
                          // child: noTodoMessageWidget(),
                          ));
            } else {
              return const Center(
                  child: Loading(
                loadingMessage: 'Загрузка',
              ));
            }
          },
        ),
      ])),
    );
  }

  void _showAddTodoSheet(BuildContext context) {
    final _categoryTitleFormController = TextEditingController();
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              color: Colors.transparent,
              child: Container(
                height: 230,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0))),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 15, top: 25.0, right: 15, bottom: 30),
                  child: ListView(
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: TextFormField(
                              controller: _categoryTitleFormController,
                              textInputAction: TextInputAction.newline,
                              maxLines: 4,
                              style: TextStyle(
                                  fontSize: 21, fontWeight: FontWeight.w400),
                              autofocus: true,
                              decoration: const InputDecoration(
                                  hintText: 'Введите название',
                                  labelText: 'Новая категория',
                                  labelStyle: TextStyle(
                                      color: Colors.teal,
                                      fontWeight: FontWeight.w500)),
                              validator: (String? value) {
                                if (value!.isEmpty) {
                                  return 'Введите название!';
                                }
                                return value.contains('') ? '' : null;
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5, top: 15),
                            child: CircleAvatar(
                              backgroundColor: Colors.teal,
                              radius: 18,
                              child: IconButton(
                                icon: Icon(
                                  Icons.save,
                                  size: 22,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  final category = Categories(
                                      title: _categoryTitleFormController
                                          .value.text);
                                  if (category.title!.isNotEmpty) {
                                    _myCategoriesBloc.addCategories(category);
                                    _myCategoriesBloc.getCategoriess();

                                    //dismisses the bottomsheet
                                    Navigator.pop(context);
                                  }
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
