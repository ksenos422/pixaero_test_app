import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app_pixaero/blocs/bloc.dart';
import 'package:test_app_pixaero/blocs/my_book_bloc.dart';
import 'package:test_app_pixaero/models/book.dart';
import 'package:test_app_pixaero/models/my_book.dart';
import 'package:test_app_pixaero/utils/divider.dart';
import 'package:test_app_pixaero/utils/log.dart';

class BooksList extends StatefulWidget {
  final List<Book> bookList;

  const BooksList(this.bookList);

  @override
  _BooksListState createState() => _BooksListState(this.bookList);
}

class _BooksListState extends State<BooksList> {
  List<Book> bookList;

  // late BookBloc _bloc;
  late MyBookBloc _blocBook;

  @override
  void initState() {
    // _bloc = BookBloc();
    _blocBook = MyBookBloc();
  }

  @override
  void dispose() {
    _blocBook.dispose();
    super.dispose();
  }

  _BooksListState(this.bookList);

  // lowerBound: 0.7,
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: bookList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 2 / 4,
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Column(children: [
                  Container(
                    height: MediaQuery.of(context).size.height * .1,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Text(
                      '${bookList[index].title}',
                      textAlign: TextAlign.center,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.teal,
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  buildDivider(),
                  Container(
                      height: MediaQuery.of(context).size.height * .25,
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Image.network(
                          '${bookList[index].image}',
                          fit: BoxFit.cover,
                        ),
                      )),
                  buildDivider(),
                  Center(
                      child: Container(
                          alignment: Alignment.bottomCenter,
                          child: IconButton(
                              onPressed: () {
                                final newMyBook = MyBook(
                                    idN: bookList[index].id,
                                    title: bookList[index].title,
                                    publisher: bookList[index].publisher,
                                    description: bookList[index].description,
                                    image: bookList[index].image,
                                    pageCount: bookList[index].pageCount,
                                    category: 0);
                                _blocBook.addMyBook(newMyBook);
                                Navigator.pop(context);
                              },
                              icon: Icon(
                                Icons.add_box_outlined,
                                size: 30,
                                color: Colors.teal,
                              )))),
                ])),
          ),
        );
      },
    );
  }
}
