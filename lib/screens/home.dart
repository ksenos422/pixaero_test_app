import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app_pixaero/blocs/categories_bloc.dart';
import 'package:test_app_pixaero/blocs/my_book_bloc.dart';
import 'package:test_app_pixaero/models/categories.dart';
import 'package:test_app_pixaero/models/my_book.dart';
import 'package:test_app_pixaero/utils/categories.dart';
import 'package:test_app_pixaero/utils/divider.dart';
import 'package:test_app_pixaero/utils/log.dart';

import 'catalog.dart';
import 'loading_screen.dart';

class HomePage extends StatelessWidget {
  final MyBookBloc myBookBloc = MyBookBloc();
  final CategoriesBloc _categoriesBloc = CategoriesBloc();

  @override
  void initState() {
    myBookBloc.getMyBooks();
    _categoriesBloc.getCategoriess();
  }

  @override
  void dispose() {
    // _bloc.dispose();
    myBookBloc.dispose();
    _categoriesBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CatalogPage(),
            ),
          ).then((value) {
            myBookBloc.getMyBooks();
          });
        },
        child: const Icon(Icons.workspaces_outline),
      ),
      body: Container(
          child: StreamBuilder(
        stream: myBookBloc.myBooks,
        builder: (BuildContext context, AsyncSnapshot<List<MyBook>> asyncSnap) {
          if (asyncSnap.hasData) {
            return asyncSnap.data!.length != 0
                ? ListView.builder(
                    itemCount: asyncSnap.data!.length,
                    itemBuilder: (context, itemPosition) {
                      MyBook myBook = asyncSnap.data![itemPosition];

                      return Container(
                        padding: EdgeInsets.all(15),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Column(children: [
                                ListTile(
                                  title: Text(
                                    '${myBook.title}',
                                    textAlign: TextAlign.center,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.teal,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                  trailing: Icon(
                                    Icons.edit,
                                    color: Colors.teal,
                                  ),
                                  onTap: () {
                                    showEditCategory(context, myBook);
                                    // showEditCategory(context, myBook);
                                  },
                                ),
                                buildDivider(),
                                Container(
                                    height:
                                        MediaQuery.of(context).size.height * .2,
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Image.network(
                                        '${myBook.image}',
                                        fit: BoxFit.contain,
                                      ),
                                    )),
                                buildDivider(),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * .1,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: Text(
                                    'Описание: ${myBook.description}',
                                    textAlign: TextAlign.center,
                                    maxLines: 10,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.teal,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ),
                                buildDivider(),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * .1,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: Text(
                                    'Категория: ${categoriesBook().where((e) => e['id'] == myBook.category).toList()[0]['title']}',
                                    textAlign: TextAlign.center,
                                    maxLines: 10,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.teal,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ),
                              ])),
                        ),
                      );
                    },
                  )
                : Container();
          } else {
            return const Center(
                child: Loading(
              loadingMessage: 'Загрузка',
            ));
          }
        },
      )),
    );
  }

  void showEditCategoris(BuildContext context, MyBook myBook) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) => Container(
                child: Column(children: <Widget>[
              StreamBuilder(
                stream: _categoriesBloc.categories,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Categories>> asyncSnap) {
                  if (asyncSnap.hasData) {
                    return asyncSnap.data!.length != 0
                        ? ListView.builder(
                            shrinkWrap: true,
                            itemCount: asyncSnap.data!.length,
                            itemBuilder: (context, itemPosition) {
                              Categories categories =
                                  asyncSnap.data![itemPosition];
                              return Container(
                                child: Card(
                                  elevation: 5,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: ListTile(
                                      title: Text(
                                        '${categories.title}',
                                        textAlign: TextAlign.center,
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.teal,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      onTap: () {},
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                        : Container(
                            child: Center(
                                // child: noTodoMessageWidget(),
                                ));
                  } else {
                    return const Center(
                        child: Loading(
                      loadingMessage: 'Загрузка',
                    ));
                  }
                },
              )
            ])));
  }

  void showEditCategory(BuildContext context, MyBook myBook) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) =>
            CupertinoActionSheet(title: const Text('Выберите категорию'),
                // message: const Text('Your options are '),
                actions: <Widget>[
                  ...categoriesBook()
                      .map((categ) => CupertinoActionSheetAction(
                            child: Text(categ['title']),
                            isDestructiveAction: false,
                            onPressed: () {
                              myBook.category = categ['id'];
                              myBookBloc.updateMyBook(myBook);
                              Navigator.pop(context, ' ');
                            },
                          ))
                      .toList(),
                  CupertinoActionSheetAction(
                    child: Text('Отмена'),
                    isDestructiveAction: true,
                    onPressed: () {
                      Navigator.pop(context, ' ');
                    },
                  )
                ]));
  }
}
