
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ApiError extends StatelessWidget {
  final String? errorMessage;

  final Function onRetryPressed;

  const ApiError({Key? key, this.errorMessage, required this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Ошибка загрузки",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),

        ],
      ),
    );
  }
}
