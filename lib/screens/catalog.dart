import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app_pixaero/api/api_response.dart';
import 'package:test_app_pixaero/models/book.dart';
import 'package:test_app_pixaero/screens/books_screen.dart';
import 'package:test_app_pixaero/utils/log.dart';

import '../blocs/bloc.dart';
import 'api_error_screen.dart';
import 'loading_screen.dart';

class CatalogPage extends StatefulWidget {
  @override
  _CatalogPageState createState() => _CatalogPageState();
}

class _CatalogPageState extends State<CatalogPage> {
  late BookBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BookBloc();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Каталог"),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: RefreshIndicator(
          onRefresh: () => _bloc.fetchBookList(),
          child: StreamBuilder<dynamic>(
            stream: _bloc.bookListStream,
            builder: (context, asyncSnap) {
              if (asyncSnap.hasData) {
                switch (asyncSnap.data.status) {
                  case Status.LOADING:
                    return Loading(
                      loadingMessage: asyncSnap.data.message,
                    );
                    break;
                  case Status.COMPLETED:
                    return BooksList(asyncSnap.data.data);
                    break;
                  case Status.ERROR:
                    return ApiError(
                      errorMessage: asyncSnap.data.message,
                      onRetryPressed: () => _bloc.fetchBookList(),
                    );
                    break;
                }
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
