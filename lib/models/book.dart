class Book {
  String? id;
  String? title;
  String? publisher;
  String? description;
  String? image;
  int? pageCount;
  // List<String>? authors;

  Book(
      {
        this.id,
        this.title,
        this.publisher,
        this.description,
        this.image,
        this.pageCount,
        // this.authors
});

  Book.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['volumeInfo']['title'];
    publisher = json['volumeInfo']['publisher'];
    description = json['volumeInfo']['description'];
    image = json['volumeInfo']['imageLinks']['thumbnail'];
    pageCount = json['volumeInfo']['pageCount'];
    // authors = json['volumeInfo']['authors'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['publisher'] = this.publisher;
    data['description'] = this.description;
    data['image'] = this.image;
    data['pageCount'] = this.pageCount;
    // data['authors'] = this.authors;

    return data;
  }
}