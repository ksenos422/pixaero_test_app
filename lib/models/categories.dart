class Categories {
  int? idN;
  String? title;

  Categories({
     this.idN,
    this.title,
  });


  factory Categories.fromDatabaseJson(Map<String, dynamic> data) => Categories(
    idN: data['id'],
    title: data['title'],
  );

  Map<String, dynamic> toDatabaseJson() => {
    "id": this.idN,
    "title": this.title,
  };
}
