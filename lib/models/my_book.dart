class MyBook {
  String? idN;
  String? title;
  String? publisher;
  String? description;
  String? image;
  int? pageCount;
  int? category = 0;

  MyBook({
    this.idN,
    this.title,
    this.publisher,
    this.description,
    this.image,
    this.pageCount,
    this.category,
});


  factory MyBook.fromDatabaseJson(Map<String, dynamic> data) => MyBook(
    idN: data['id'].toString(),
    title: data['title'],
    publisher: data['publisher'],
    description: data['description'],
    image: data['image'],
    pageCount: data['pageCount'],
    category: data['category'],
  );

  Map<String, dynamic> toDatabaseJson() => {
    "idN": this.idN,
    "title": this.title,
    "publisher": this.publisher,
    "description": this.description,
    "image": this.image,
    "pageCount": this.pageCount,
    "category": this.category,
  };
}
