
import 'package:test_app_pixaero/models/book.dart';
import 'package:test_app_pixaero/utils/log.dart';

class BookResponse {
  List? totalResults;
  List<Book>? results;

  BookResponse.fromJson(Map<String, dynamic> json) {
    console.log(json);
    totalResults = json['items'];
    if (json['items'] != null) {
      results = <Book>[];
      json['items'].forEach((v) {
        results!.add(new Book.fromJson(v));
      });
    }
  }
}