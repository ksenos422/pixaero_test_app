import 'package:test_app_pixaero/models/categories.dart';
import 'package:test_app_pixaero/models/my_book.dart';
import 'package:test_app_pixaero/utils/log.dart';

import '../db.dart';

class CategoriesDBO {
  final dbProvider = DatabaseProvider.dbProvider;

  Future<int> createCategories(Categories category) async {
    final db = await dbProvider.database;
    var result = db!.insert(categoryTable, category.toDatabaseJson());
    return result;
  }

  Future<List<Categories>> getCategories({List<String>? columns, String? query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result =[];
    if (query != null) {
      if (query.isNotEmpty) {
        result = await db!.query(categoryTable,
            columns: columns,
        );
      }
    } else {
      result = await db!.query(categoryTable, columns: columns);
    }


    List<Categories> categorys = result.isNotEmpty
        ? result.map((item) => Categories.fromDatabaseJson(item)).toList()
        : [];
    return categorys;
  }

  Future<int> updateCategories(Categories category) async {
    final db = await dbProvider.database;

    var result = await db!.update(categoryTable, category.toDatabaseJson(),
        where: "id = ?", whereArgs: [category.idN]);

    return result;
  }

  Future<int> deleteCategories(int id) async {
    final db = await dbProvider.database;
    var result = await db!.delete(categoryTable, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  Future deleteAllCategories() async {
    final db = await dbProvider.database;
    var result = await db!.delete(
      categoryTable,
    );

    return result;
  }
}
