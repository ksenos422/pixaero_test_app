
import 'package:test_app_pixaero/models/my_book.dart';

import 'dbo_my_book.dart';

class MyBookOperations {
  final myBookDao = MyBooksDBO();

  Future getAllMyBooks({String? query}) => myBookDao.getMyBooks(query: query);
  Future getAllMyBooksByCategory({String? query}) => myBookDao.getMyBooksByCategory(query: query);

  Future insertMyBook(MyBook myBook) => myBookDao.createMyBook(myBook);

  Future updateMyBook(MyBook myBook) => myBookDao.updateMyBook(myBook);

  Future deleteMyBookById(int id) => myBookDao.deleteMyBook(id);

}
