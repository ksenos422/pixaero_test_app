
import 'package:test_app_pixaero/models/categories.dart';
import 'dbo_categories.dart';
import 'dbo_my_book.dart';

class CategoriesOperations {
  final categoryDao = CategoriesDBO();

  Future getAllCategories({String? query}) => categoryDao.getCategories(query: query);

  Future insertCategories(Categories category) => categoryDao.createCategories(category);

  Future updateCategories(Categories category) => categoryDao.updateCategories(category);

  Future deleteCategoriesById(int id) => categoryDao.deleteCategories(id);

}
