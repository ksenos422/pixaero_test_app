import 'package:test_app_pixaero/models/my_book.dart';
import 'package:test_app_pixaero/utils/log.dart';

import '../db.dart';

class MyBooksDBO {
  final dbProvider = DatabaseProvider.dbProvider;

  //Adds new MyBook records
  Future<int> createMyBook(MyBook myBook) async {
    final db = await dbProvider.database;
    var result = db!.insert(mybooktable, myBook.toDatabaseJson());
    return result;
  }

  Future<List<MyBook>> getMyBooks(
      {List<String>? columns, String? query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = [];
    if (query != null) {
      if (query.isNotEmpty) {
        result = await db!.query(
          mybooktable,
          columns: columns,
          // where: 'description LIKE ?',
          // whereArgs: ["%$query%"]
        );
      }
    } else {
      result = await db!.query(mybooktable, columns: columns);
    }
    List<MyBook> myBooks = result.isNotEmpty
        ? result.map((item) => MyBook.fromDatabaseJson(item)).toList()
        : [];
    return myBooks;
  }

  Future<List<MyBook>> getMyBooksByCategory(
      {List<String>? columns, String? query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = [];
    if (query != null) {
      if (query.isNotEmpty) {
        result = await db!.query(mybooktable,
            columns: columns, where: 'category = ?', whereArgs: [query]);
      }
    } else {
      result = await db!.query(mybooktable, columns: columns);
    }
    List<MyBook> myBooks = result.isNotEmpty
        ? result.map((item) => MyBook.fromDatabaseJson(item)).toList()
        : [];
    return myBooks;
  }

  //Update MyBook record
  Future<int> updateMyBook(MyBook myBook) async {
    final db = await dbProvider.database;

    var result = await db!.update(mybooktable, myBook.toDatabaseJson(),
        where: "id = ?", whereArgs: [myBook.idN]);

    return result;
  }

  //Delete MyBook records
  Future<int> deleteMyBook(int id) async {
    final db = await dbProvider.database;
    var result =
        await db!.delete(mybooktable, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  //We are not going to use this in the demo
  Future deleteAllMyBooks() async {
    final db = await dbProvider.database;
    var result = await db!.delete(
      mybooktable,
    );

    return result;
  }
}
