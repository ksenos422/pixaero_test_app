import 'dart:convert';

import 'package:test_app_pixaero/models/book.dart';
import 'package:test_app_pixaero/utils/constants.dart';
import 'package:test_app_pixaero/utils/log.dart';

import 'api_base_helper.dart';
import 'book_get_fetch.dart';
import 'dart:developer' as developer;

class BookApi {

  ApiBaseHelper _helper = ApiBaseHelper();

  Future<List<Book>> fetchBookList() async {
    final response = await _helper.get(Constants.apiBaseUrl);
    Map<String, dynamic> res = response;
    return BookResponse.fromJson(res).results ?? [];
  }
}