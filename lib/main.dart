import 'package:flutter/material.dart';
import 'package:test_app_pixaero/screens/about.dart';
import 'package:test_app_pixaero/screens/catalog.dart';
import 'package:test_app_pixaero/screens/categories_books.dart';
import 'package:test_app_pixaero/screens/clipper_drawer.dart';
import 'package:test_app_pixaero/screens/home.dart';
import 'package:test_app_pixaero/screens/splash.dart';
import 'package:test_app_pixaero/utils/divider.dart';

import 'package:test_app_pixaero/utils/style.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Books App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Styles.primaryColor, primarySwatch: Colors.teal),
      initialRoute: 'splash/',
      routes: {
        '/': (context) => MyHomePage(),
        'splash/': (context) => Splash(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListTile(
              title: Text('Мои книги'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text('Библиотека'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CatalogPage(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text('Категории'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CategoriesBooks(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      drawer: _buildDrawer(context),
    );
  }
}

_buildDrawer(BuildContext context) {
  return ClipPath(
      clipper: OvalRightBorderClipper(),
      child: Drawer(
          child: Container(
              decoration: const BoxDecoration(
                color: Colors.teal,
              ),
              child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 50),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                            'О приложении',
                            style: TextStyle(color: Colors.white),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AboutApp(),
                              ),
                            );
                          },
                        ),
                        buildDivider(),
                      ])))));
}
