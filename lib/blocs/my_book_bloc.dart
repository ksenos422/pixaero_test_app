
import 'dart:async';

import 'package:test_app_pixaero/api/book_operations.dart';
import 'package:test_app_pixaero/models/my_book.dart';
import 'package:test_app_pixaero/utils/log.dart';

class MyBookBloc {

  final _myBook = MyBookOperations();

  final _myBookController = StreamController<List<MyBook>>.broadcast();

  get myBooks => _myBookController.stream;

  MyBookBloc() {
    getMyBooks();
  }

  getMyBooks({String? query}) async {
    _myBookController.sink.add(await _myBook.getAllMyBooks(query: query));
  }
  getMyBooksByCategory(String? query) async {
    _myBookController.sink.add(await _myBook.getAllMyBooksByCategory(query: query));
  }


  addMyBook(MyBook myBook) async {
    await _myBook.insertMyBook(myBook);
    getMyBooks();
  }

  updateMyBook(MyBook myBook) async {
    await _myBook.updateMyBook(myBook);
    getMyBooks();
  }

  deleteMyBookById(int id) async {
    _myBook.deleteMyBookById(id);
    getMyBooks();
  }

  dispose() {
    _myBookController.close();
  }
}
