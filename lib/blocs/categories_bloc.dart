
import 'dart:async';

import 'package:test_app_pixaero/api/book_operations.dart';
import 'package:test_app_pixaero/api/category_operations.dart';
import 'package:test_app_pixaero/models/categories.dart';
import 'package:test_app_pixaero/models/my_book.dart';
import 'package:test_app_pixaero/utils/log.dart';

class CategoriesBloc {

  final _categories = CategoriesOperations();

  final _categoriesController = StreamController<List<Categories>>.broadcast();

  get categories => _categoriesController.stream;

  CategoriesBloc() {
    getCategoriess();
  }

  getCategoriess({String? query}) async {
    console.log('пришел в getCategoriess');
    _categoriesController.sink.add(await _categories.getAllCategories(query: query));
  }



  addCategories(Categories categories) async {
      await _categories.insertCategories(categories);
    getCategoriess();
  }

  updateCategories(Categories categories) async {
    await _categories.updateCategories(categories);
    getCategoriess();
  }

  deleteCategoriesById(int id) async {
    _categories.deleteCategoriesById(id);
    getCategoriess();
  }

  dispose() {
    _categoriesController.close();
  }
}
