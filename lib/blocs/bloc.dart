import 'dart:async';

import '../api/api_response.dart';
import '../api/book_api.dart';
import '../models/book.dart';

class BookBloc {
  late BookApi _bookApi;

  late StreamController _bookListController;

  StreamSink<dynamic> get bookListSink =>
      _bookListController.sink;

  Stream<dynamic> get bookListStream =>
      _bookListController.stream;

  BookBloc() {
    _bookListController = StreamController<dynamic>();
    _bookApi = BookApi();
    fetchBookList();
  }

  fetchBookList() async {
    bookListSink.add(ApiResponse.loading(''));
    try {
      List<Book>? books = await _bookApi.fetchBookList();
      bookListSink.add(ApiResponse.completed(books));
    } catch (e) {
      bookListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _bookListController.close();
  }
}